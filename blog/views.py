from django.shortcuts import render
from django.views import generic
from .models import Blog, BlogComment, Tag, BlogImage
from .forms import NewCommentForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


# Create your views here.
"""
class BlogList(generic.ListView):
    queryset = Blog.objects.filter(status=1).order_by('-created_on')
    template_name = 'index.html'
"""
def blog_list(request):
	blogs = Blog.objects.filter(status=1)
	p = Paginator(blogs, 2)		# create a paginator object
	page_number = request.GET.get('page')
	try:
		page_obj = p.get_page(page_number)	#return the desired page object
	except PageNotAnInteger:			# if page_number isn't an integer then assign 1st page
		page_obj = p.page(1)
	except EmptyPage:				# if empty page=>return last page
		page_obj = p.page(p.num_pages)
	lucky_num = gather_nums()
	return render(request, 'index.html', {'blog_list':blogs, 'r_num':lucky_num, 'page_obj': page_obj})
    
class BlogDetail(generic.DetailView):
	model = Blog
	template_name = 'blog_detail.html'
	def get_context_data(self, **kwargs):
		data = super().get_context_data(**kwargs)

		comments_connected = BlogComment.objects.filter(blog_connected = self.get_object()).order_by('-date_created')
		data['comments'] = comments_connected
		if self.request.user.is_authenticated:
			data['comment_form'] = NewCommentForm(instance=self.request.user)
		### ------ Add images belongs to the blog -------
		#### Filter images with blog_id, refer to:https://stackoverflow.com/questions/51892193/django-filter-in-a-view
		blog_images = BlogImage.objects.filter(blog_id=self.kwargs['pk'])
		data.update({
			'blog_images': blog_images,
		})
		## ----------------------------------------------
		return data
        
	def post(self, request, *args, **kwargs):
		new_comment = BlogComment(comment=request.POST.get('comment'),
                	author=self.request.user,
        	        blog_connected=self.get_object())
		new_comment.save()
		return self.get(self, *args, **kwargs)
"""
def get_lottery(request):
	get_lottery = lottery_list()
	# Pass the imported dict to the template
	return  render(request, 'lottery.html', {'d': get_lottery})

def get_random(request):
	luckynum = gather_nums()
	# Pass the imported dict to the template
	return  render(request, 'lottery.html', {'r': luckynum})
"""


###---       Web scraping area      ---###
#import requests
#from bs4 import BeautifulSoup as bs
#import re
#from django.http import HttpResponse
#from fake_useragent import UserAgent


## New web scraping method , read a single file and generate random numbers
import json
import random
from random import randrange
import os

## Open the record file
def gather_nums():
	input_file = os.path.realpath("frequent_nums.dat")
	with open(input_file, "r") as f:
		data = f.read()
		nums = json.loads(data)
	first_num = randrange(1, nums[0])
	select_lst = []
	select_lst.append(first_num)
	## Select if diff of 2 number is larger than 4
	for i in range(len(nums)):
		if nums[i]-select_lst[len(select_lst)-1] > 5:
			select_lst.append(nums[i])

	random_lst = []
	for j in range(len(select_lst)-1):
		random_num = random.randint(select_lst[j], select_lst[j+1])
		random_lst.append(random_num)
	#random_lst = random.sample(select_lst, 3)	#This time we select 3 random number from the select_lst
	# Now, merge the list and remove duplicated numbers
	list3 = random_lst + select_lst
	final_lst = list(sorted(set(list3)))
	
	final_dict = {"Most frequent number(s)":[], "Lucky number(s)":[]}
	final_dict.update({"Most frequent number(s)":nums, "Lucky number(s)":final_lst})
	return final_dict
