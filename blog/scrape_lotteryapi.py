#!/usr/bin/python
#
from datetime import date
from urllib.request import urlopen
import json
import os

def lottery_list():
    ## Define URL for scraping lottery numbers
    today = date.today()
    month = '{:02d}'.format(today.month)
    URL = "https://api.taiwanlottery.com/TLCAPIWeB/Lottery/Lotto649Result?period&month=2025-" + str(month) +"&pageNum=1&pageSize=8"
#print(URL)
    rsp = urlopen(URL)
    print(rsp)
    data_json = json.loads(rsp.read())
#draw_size = data_json['content']
    lottery_dict = {}
    lottery_number = len(data_json['content']['lotto649Res'])
#    print(lottery_number)
    for i in range(lottery_number):
#        print(data_json['content']['lotto649Res'][i]['drawNumberSize'])
        new_element = {i:data_json['content']['lotto649Res'][i]['drawNumberSize']}
        lottery_dict.update(new_element)
#    print(lottery_dict)
## Create a new list to calculate numbers appearing count
    new_lst = [0]*50	# calculating number's apperance
    freq_lst = []		# store number satisfying the condition
    for lst in lottery_dict.values():
        for j in range(50):
            for k in lst:
                if k == j+1:
                    new_lst[j] = new_lst[j] + 1	# Count
    
    for l in range(len(new_lst)):
		### if the number repeating more than 2 times
        if new_lst[l] > 2:
            freq_lst.append(l+1)

    number_file = os.getcwd() + '/blog' + '/frequent_nums.dat'
    #number_file = os.getcwd() + '/frequent_nums.txt'
	##print(number_file)
    with open(number_file, "w") as fdict:
        fdict.write(str(freq_lst))
        fdict.close()
    print(freq_lst)
    return freq_lst

if __name__ == '__main__':
	lottery_list()
