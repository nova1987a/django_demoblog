from django.contrib import admin
from .models import Blog,Tag, BlogImage

# Register your models here.
class BlogImageAdmin(admin.StackedInline):
	model = BlogImage

class BlogAdmin(admin.ModelAdmin):
	list_display = ('title', 'status', 'created_on')
	search_field = ['title', 'content']
	inlines = [BlogImageAdmin]

class BlogImageAdmin(admin.ModelAdmin):
	pass

admin.site.register(BlogImage, BlogImageAdmin)
admin.site.register(Blog, BlogAdmin)
admin.site.register(Tag)

