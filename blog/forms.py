from .models import BlogComment
from django import forms
from django.forms import TextInput

class NewCommentForm(forms.ModelForm):
	comment = forms.CharField(widget=forms.TextInput(attrs={'placehjolder':'Leave your comment', 'style': 'width: 400px; height:200px;','class': 'form-control'}))
	class Meta:
		model = BlogComment
		fields = ['comment']
