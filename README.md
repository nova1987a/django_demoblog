<p align="center"><a href="https://www.djangoproject.com" target="_blank"><img src="https://static.djangoproject.com/img/logos/django-logo-positive.png" width="400" alt="Django Logo"></a></p>

[[_TOC_]]

## Personal demo blog based on Django framework

- {+ The folder "blog/" is based on Django class-based API ; +}

- folder ` "news/" `  is based on djangorestframework , refer to [iThelp](https://ithelp.ithome.com.tw/articles/10234100)

- {- Idea of pagination is referred to -} [Here](https://www.geeksforgeeks.org/how-to-add-pagination-in-django-project/)

## Some Notes
- About  images uploads, refer to [Here](https://soshace.com/upload-multiple-images-to-a-django-model-without-plugins/) and [Here](https://stackoverflow.com/questions/71560888/django-trying-to-render-the-data-that-i-added-to-a-detailview-via-get-context-da)

![Blog demo -- snapshot](https://gitlab.com/nova1987a/django_demoblog/-/raw/main/djdemo_snap.jpg?ref_type=heads)
